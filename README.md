# What is LazInstallerHelper?

LazInstallerHelper is an IDE plugin for the [Lararus IDE](http://www.lazarus-ide.org/) that adds support for some Windows Installer toolkits to the IDE.

Currently supported are:
* [InnoSetup](http://jrsoftware.org/isinfo.php) (version > 5.0.5)
* [NSIS](http://nsis.sourceforge.net/Main_Page)
* [WiX Toolkit](http://wixtoolset.org/)
