{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Lazarus IDE extension to compile installer scripts.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-02  pk  10m  Implemented Build Specific submenu.
// 2017-06-01  pk   1h  Linked to codesigning through third package.
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

// TODO : Project Inspector Files Context menu

unit InstallerHelper.Menu;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Windows,
   Laz2_XMLCfg,
   MenuIntf,
   MacroIntf,
   LazarusPackageIntf,
   IDEOptionsIntf,
   ProjectIntf,
   LazIDEIntf,
   SrcEditorIntf,
   IDEMsgIntf,
   IDEExternToolIntf,
   ProjectResourcesIntf,
   InstallerHelper.Options;

procedure Register;

const
   SCodeSigningInterface = '{68BA16C3-CABE-4169-B0DD-388F513FFC48}';

type
   // see CodeSigningHelper.Menu.pas for the original
   ICodeSigningInterface = interface
      [SCodeSigningInterface]
      function CertificateSignExecutable(AFilename: string; AAllowProjectSpecificOptions: boolean = True; ACustomDescription: string = ''; AViewName: string = ''): boolean;
      function CertificateVerifyExecutable(AFilename: string; AAllowProjectSpecificOptions: boolean = True; AViewName: string = ''): boolean;
      function GnuPGSignFile(AFilename: string; AAllowProjectSpecificOptions: boolean = True; AViewName: string = ''): boolean;
      function GnuPGVerifyFile(AFilename: string; AAllowProjectSpecificOptions: boolean = True; AViewName: string = ''): boolean;
   end;

type
   TInstallerType = (itNone, itInnoSetup, itNSIS, itWiX);

   TInstallerBuildingFinishedEvent = procedure(AScriptFilename, AOutputFilename, AViewName: string) of object;
   TInstallerBuildingFinishedEvents = array of TInstallerBuildingFinishedEvent;

   { TInstallerHelperTool }

   TInstallerHelperTool = class(TAbstractExternalTool)
   protected
      procedure CreateView;
      //function GetExecuteAfter(Index: integer): TAbstractExternalTool; override;
      //function GetExecuteBefore(Index: integer): TAbstractExternalTool; override;
      procedure DoExecute; override;
   public
      //procedure Execute; override;
      //procedure Terminate; override;
      //procedure WaitForExit; override;
   end;

   { TInstallerHelper }

   TInstallerHelper = class
   private
   class var FInstance: TInstallerHelper;
   private
      FBuildingFinishedEvents: TInstallerBuildingFinishedEvents;
      FMenuSectionInstallerBuilding: TIDEMenuSection;
      FMenuItemSourceEditorBuild: TIDEMenuItem;
      procedure AddHandlers();
      procedure RemoveHandlers();
      function IdentifyFileType(AFilename: string): TInstallerType;
      function GetCompiledFileProjectVersion(out AVersion: string): boolean;
      function ExtractVersionFromProjectInformation(AFilename: string; AIncludeBuild: boolean = False): string;
      function BuildInnoSetupInstallerUsingLibraryForProjectFile(AFile: TLazProjectFile): boolean;
      function BuildNSISInstallerForProjectFile(AFile: TLazProjectFile): boolean;
      function BuildWiXInstallerForProjectFile(AFile: TLazProjectFile): boolean;
      function CallBuildingFinishedHandlers(AScriptFilename, AOutputFilename, AViewName: string): integer;
   private
      FProjectProductVersion: string;
      procedure DoIDEClose({%H-}Sender: TObject);
      procedure DoCreateInstallers({%H-}Sender: TObject);
      procedure DoCreateSpecificInstaller({%H-}Sender: TObject);
      procedure DoCreateInstallerForEditorFile({%H-}Sender: TObject);
      procedure DoListInstallersToSubMenu({%H-}Sender: TObject);
      procedure DoProjectBuildingFinished({%H-}ASender: TObject; ABuildSuccessful: boolean);
      procedure DoSourceEditorWindowActivate({%H-}Sender: TObject);
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: TInstallerHelper;
   public
      constructor Create;
      destructor Destroy; override;
      class procedure DumpFileInformation(AText, AFilename, AView: string);
      function AddBuildingFinishedHandler(AHandler: TInstallerBuildingFinishedEvent): integer;
      procedure RemoveBuildingFinishedHandler(AHandler: TInstallerBuildingFinishedEvent);
      procedure CreateMainMenuSubMenu();
   public
      procedure CreateProjectFileInstallers(AHonorAutoBuildOptions: boolean = False);
      function BuildInnoSetupInstallerUsingLibrary(AFilename: string; AUpdateVersion: boolean = False; ANewVersion: string = ''): boolean;
      function BuildNSISInstaller(AFilename: string): boolean;
      function BuildWiXInstaller(AFilename: string): boolean;
      function BuildInstaller(AFilename: string): boolean;
      property ProjectProductVersion: string read FProjectProductVersion;
   end;

var
   InstallerOptionGroup: integer;

implementation

uses
   FileInfo,
   IDEOptEditorIntf,
   InstallerHelper.InnoSetup.Lib,
   InstallerHelper.InnoSetup.Options,
   InstallerHelper.InnoSetup.Frame,
   InstallerHelper.NSIS.CommandLine,
   InstallerHelper.NSIS.Options,
   InstallerHelper.NSIS.Frame,
   InstallerHelper.WiX.CommandLine,
   InstallerHelper.WiX.Options,
   InstallerHelper.WiX.Frame;

procedure Register;
begin
   InstallerOptionGroup := GetFreeIDEOptionsGroupIndex(GroupEditor);
   RegisterIDEOptionsGroup(InstallerOptionGroup, TInstallerOptions);
   RegisterIDEOptionsEditor(InstallerOptionGroup, TFrameInstallerOptionsInnoSetup, InstallerOptionGroup + 1);
   RegisterIDEOptionsEditor(InstallerOptionGroup, TFrameInstallerOptionsNSIS, InstallerOptionGroup + 2);
   RegisterIDEOptionsEditor(InstallerOptionGroup, TFrameInstallerOptionsWiX, InstallerOptionGroup + 3);
   TInstallerHelper.Instance.CreateMainMenuSubMenu();
end;

type
   TInstance = function: TPersistent of object;

function FindPersistentObjectUsingClassMethod(AClassName: string; AInstanceMethodName: string = 'Instance'): TPersistent;
var
   c: TPersistentClass;
   m: TMethod;
begin
   Result := nil;
   c := FindClass(AClassName);
   if not Assigned(c) then begin
      Exit;
   end;
   m.Code := c.MethodAddress(AInstanceMethodName);
   if Assigned(m.Code) then begin
      m.Data := nil;
      Result := TInstance(m)();
   end;
end;

function FindNamedInterfaceForPersistentObjectUsingClassMethod(AClassName, AInterfaceGUIDString: string; AInstanceMethodName: string = 'Instance'): IUnknown;
var
   o: TPersistent;
begin
   Result := nil;
   o := FindPersistentObjectUsingClassMethod(AClassName, AInstanceMethodName);
   if not Assigned(o) then begin
      Exit;
   end;
   o.GetInterface(AInterfaceGUIDString, Result);
end;

function FindCodeSigningInterface: ICodeSigningInterface;
begin
   Result := ICodeSigningInterface(FindNamedInterfaceForPersistentObjectUsingClassMethod('TCodeSigningHelper', SCodeSigningInterface, 'Instance'));
end;

{ TInstallerHelperTool }

procedure TInstallerHelperTool.CreateView;
var
   View: TExtToolView;
begin
   if ViewCount > 0 then exit;
   if (ViewCount = 0) and (ParserCount > 0) and (IDEMessagesWindow <> nil) then begin
      View := IDEMessagesWindow.CreateView('Installer Creation');
      if View <> nil then begin
         AddView(View);
      end;
   end;
end;

procedure TInstallerHelperTool.DoExecute;
begin
   CreateView;
end;

{ TInstallerHelper }

class function TInstallerHelper.Instance: TInstallerHelper;
begin
   if not Assigned(FInstance) then begin
      FInstance := TInstallerHelper.Create;
   end;
   Result := FInstance;
end;

class procedure TInstallerHelper.DumpFileInformation(AText, AFilename, AView: string);
type
   TLangAndCodepage = packed record
      wLanguage, wCodepage: word;
   end;

   PLangAndCodepage = ^TLangAndCodepage;

   function GetVersionField(const AFilename, AField: string; out Data: string): boolean;
   var
      dwVersionSize: DWord;
      hVersion: cardinal;
      pVersionBlock, pVersionData: Pointer;
      plc, plcIterator: PLangAndCodepage;
      lc: TLangAndCodepage;
      i: cardinal;
      psVersionInfo: Pointer;
      pcVersionInfo: pwidechar;
      cSize: cardinal;
      size: cardinal;
      sIdentifier: WideString;
   begin
      Result := False;
      {$IFDEF MSWindows}
      try
         hVersion := 0;
         dwVersionSize := GetFileVersionInfoSizeW(pwidechar(WideString(AFilename)), hVersion);
         if dwVersionSize > 0 then begin
            GetMem(pVersionBlock, dwVersionSize);
            try
               if GetFileVersionInfoW(pwidechar(WideString(AFilename)), hVersion, dwVersionSize, pVersionBlock) then begin
                  size := 0;
                  pVersionData := nil;
                  if VerQueryValue(pVersionBlock, '\\VarFileInfo\\Translation', pVersionData, size) then begin
                     plc := pVersionData;
                     for i := 0 to (size div SizeOf(TLangAndCodepage)) - 1 do begin
                        plcIterator := Pointer(NativeUInt(plc) + (i * SizeOf(TLangAndCodepage)));
                        lc := plcIterator^;
                        psVersionInfo := nil;
                        sIdentifier := '\\StringFileInfo\\' + WideString(IntToHex(lc.wLanguage, 4)) + WideString(IntToHex(lc.wCodepage, 4)) + '\\' + WideString(AField);
                        cSize := 0;
                        if VerQueryValueW(pVersionBlock, pwidechar(sIdentifier), psVersionInfo, cSize) then begin
                           pcVersionInfo := psVersionInfo;
                           if Length(pcVersionInfo) > 0 then begin
                              Data := string(WideString(pcVersionInfo));
                              Result := True;
                              Break;
                           end;
                        end else begin
                           AddIDEMessage(mluError, Format('Unable to VerQueryValue for block %d in file %s', [i, AFilename]), '', 0, 0, '');
                        end;
                     end;
                  end else begin
                     AddIDEMessage(mluError, Format('Unable to VerQueryValue for translation list in file %s', [AFilename]), '', 0, 0, '');
                  end;
               end else begin
                  AddIDEMessage(mluError, Format('Unable to GetFileVersionInfoW for %s', [AFilename]), '', 0, 0, '');
               end;
            finally
               FreeMem(pVersionBlock, dwVersionSize);
            end;
         end;
      except
         on E: Exception do begin
            AddIDEMessage(mluError, Format('There was an issue retrieving file properties for %s: %s', [AFilename, E.Message]));
            Result := False;
         end;
      end;
      {$ENDIF MSWindows}
   end;

var
   sMessage: string;
   sDetails, sVersion: string;
begin
   if GetVersionField(AFilename, 'FileVersion', sVersion) then begin
      sDetails := Format('version %s', [sVersion]);
   end else begin
      sDetails := 'unknown file version';
   end;
   sMessage := Format(AText, [AFilename, sDetails]);
   AddIDEMessage(mluNote, sMessage, '', 0, 0, AView);
end;

function TInstallerHelper.IdentifyFileType(AFilename: string): TInstallerType;
begin
   Result := itNone;
   if SameText(RightStr(AFilename, 4), '.iss') then begin
      Result := itInnoSetup;
      Exit;
   end;
   if SameText(RightStr(AFilename, 4), '.nsi') then begin
      Result := itNSIS;
      Exit;
   end;
   if SameText(RightStr(AFilename, 4), '.wxs') then begin
      Result := itWiX;
      Exit;
   end;
end;

(*
 * credits to cdesim, https://forum.lazarus.freepascal.org/index.php/topic,56068.msg416767.html#msg416767
 *)
function TInstallerHelper.GetCompiledFileProjectVersion(out AVersion: string): boolean;
var
   fvi: TFileVersionInfo;
   s: string;
   sView: string;
begin
   sView := 'Retrieving product version to update installer scripts';
   try
      fvi := TFileVersionInfo.Create(nil);
      try
         //s := LazarusIDE.ActiveProject.LazBuildModes.BuildModes[LazarusIDE.ActiveProject.LazBuildModes.IndexOf(LazarusIDE.ActiveProject.ActiveBuildModeID)].LazCompilerOptions.TargetFileName;
         s := '$(TargetFile)';
         IDEMacros.SubstituteMacros(s);
         fvi.Filename := s;
         fvi.ReadFileInfo;
         AVersion := fvi.VersionStrings.Values['ProductVersion'];
      finally
         fvi.Free;
      end;
      Result := Length(AVersion) > 0;
      if Result then begin
         AddIDEMessage(mluNote, Format('Retrieved product version %s from compiled file %s.', [AVersion, s]), '', 0, 0, sView);
      end else begin
         AddIDEMessage(mluError, Format('Unable to get product version from compiled file %s.', [s]), '', 0, 0, sView);
      end;
   except
      Result := False;
   end;
end;

function TInstallerHelper.BuildInnoSetupInstallerUsingLibraryForProjectFile(AFile: TLazProjectFile): boolean;
var
   sNewVersion: string;
begin
   sNewVersion := FProjectProductVersion;
   if Length(sNewVersion) = 0 then begin
      GetCompiledFileProjectVersion(sNewVersion);
   end;
   if Length(sNewVersion) > 0 then begin
      Result := BuildInnoSetupInstallerUsingLibrary(AFile.Filename, True, sNewVersion);
   end else begin
      Result := BuildInnoSetupInstallerUsingLibrary(AFile.Filename);
   end;
end;

function TInstallerHelper.BuildNSISInstallerForProjectFile(AFile: TLazProjectFile): boolean;
begin
   Result := BuildNSISInstaller(AFile.Filename);
end;

function TInstallerHelper.BuildWiXInstallerForProjectFile(AFile: TLazProjectFile): boolean;
begin
   Result := BuildWiXInstaller(AFile.Filename);
end;

(*
 * credits to GetMem, https://forum.lazarus.freepascal.org/index.php/topic,56068.msg416782.html#msg416782
 *)
function TInstallerHelper.ExtractVersionFromProjectInformation(AFilename: string; AIncludeBuild: boolean = False): string;

   function VersionBound(const AVersion: integer): integer;
   begin
      if AVersion > 9999 then
         Result := 9999
      else if AVersion < 0 then
         Result := 0
      else
         Result := AVersion;
   end;

var
   XML: TXMLConfig;
   bUseVersionInfo: boolean;
   iMajor, iMinor, iRelease, iBuild: integer;
   sBasePath: string;
   sView: string;
begin
   Result := '';
   sView := 'Retrieving product version to update installer scripts';
   XML := TXMLConfig.Create(AFilename);
   try
      sBasePath := 'ProjectOptions/VersionInfo/';
      bUseVersionInfo := XML.GetValue(sBasePath + 'UseVersionInfo/Value', False);
      if bUseVersionInfo then begin
         iMajor := VersionBound(XML.GetValue(sBasePath + 'MajorVersionNr/Value', 0));
         iMinor := VersionBound(XML.GetValue(sBasePath + 'MinorVersionNr/Value', 0));
         iRelease := VersionBound(XML.GetValue(sBasePath + 'RevisionNr/Value', 0));
         iBuild := VersionBound(XML.GetValue(sBasePath + 'BuildNr/Value', 0));
         Result := IntToStr(iMajor) + '.' + IntToStr(iMinor) + '.' + IntToStr(iRelease);
         if AIncludeBuild then begin
            Result += '.' + IntToStr(iBuild);
         end;
      end;
      AddIDEMessage(mluNote, Format('Retrieved product version %s from projection information %s.', [Result, AFilename]), '', 0, 0, sView);
   finally
      XML.Free;
   end;
end;

procedure TInstallerHelper.CreateProjectFileInstallers(AHonorAutoBuildOptions: boolean);
var
   i, iCount: integer;
   p: TLazProject;
   ft: TInstallerType;
begin
   FProjectProductVersion := '';
   if (not Assigned(LazarusIDE)) or (not Assigned(LazarusIDE.ActiveProject)) then begin
      Exit;
   end;
   p := LazarusIDE.ActiveProject;
   iCount := 0;
   FProjectProductVersion := ExtractVersionFromProjectInformation(p.ProjectInfoFile);
   for i := 0 to Pred(p.FileCount) do begin
      if p.Files[i].IsPartOfProject then begin
         ft := IdentifyFileType(p.Files[i].Filename);
         case ft of
            itInnoSetup:
            begin
               if (not AHonorAutoBuildOptions) or (TInstallerOptions(TInstallerOptions.GetInstance).InnoSetup.AutoCreate) then begin
                  BuildInnoSetupInstallerUsingLibraryForProjectFile(p.Files[i]);
                  Inc(iCount);
               end;
            end;
            itNSIS:
            begin
               if (not AHonorAutoBuildOptions) or (TInstallerOptions(TInstallerOptions.GetInstance).NSIS.AutoCreate) then begin
                  BuildNSISInstallerForProjectFile(p.Files[i]);
                  Inc(iCount);
               end;
            end;
            itWiX:
            begin
               if (not AHonorAutoBuildOptions) or (TInstallerOptions(TInstallerOptions.GetInstance).WiX.AutoCreate) then begin
                  BuildWiXInstallerForProjectFile(p.Files[i]);
                  Inc(iCount);
               end;
            end;
            else
            begin
               // this file is not an installer file, which is quite okay
            end;
         end;
      end;
   end;
   if iCount = 0 then begin
      AddIDEMessage(mluHint, 'No installers found in project.');
   end;
end;

function TInstallerHelper.CallBuildingFinishedHandlers(AScriptFilename, AOutputFilename, AViewName: string): integer;
var
   h: TInstallerBuildingFinishedEvent;
begin
   Result := 0;
   for h in FBuildingFinishedEvents do begin
      h(AScriptFilename, AOutputFilename, AViewName);
      Inc(Result);
   end;
end;

procedure TInstallerHelper.DoIDEClose(Sender: TObject);
begin
   RemoveHandlers;
end;

procedure TInstallerHelper.DoCreateInstallers(Sender: TObject);
begin
   IDEMessagesWindow.Clear;
   CreateProjectFileInstallers(False);
end;

procedure TInstallerHelper.DoCreateSpecificInstaller(Sender: TObject);
var
   mi: TIDEMenuItem;
begin
   if Sender is TIDEMenuItem then begin
      mi := TIDEMenuItem(Sender);
      BuildInstaller(mi.Caption);
   end;
end;

procedure TInstallerHelper.DoCreateInstallerForEditorFile(Sender: TObject);
var
   edit: TSourceEditorInterface;
begin
   edit := SourceEditorManagerIntf.ActiveEditor;
   if Assigned(edit) then begin
      BuildInstaller(edit.FileName);
   end;
end;

procedure TInstallerHelper.DoListInstallersToSubMenu(Sender: TObject);
var
   i: integer;
   p: TLazProject;
   ft: TInstallerType;
   mi: TIDEMenuItem;
   slFiles: TStringList;
begin
   if (not Assigned(LazarusIDE)) or (not Assigned(LazarusIDE.ActiveProject)) or (not Assigned(FMenuSectionInstallerBuilding)) then begin
      Exit;
   end;
   p := LazarusIDE.ActiveProject;
   slFiles := TStringList.Create;
   try
      for i := 0 to Pred(p.FileCount) do begin
         ft := IdentifyFileType(p.Files[i].Filename);
         if ft <> itNone then begin
            slFiles.Add(p.Files[i].Filename);
         end;
      end;
      while FMenuSectionInstallerBuilding.Count < Succ(slFiles.Count) do begin
         RegisterIDEMenuCommand(FMenuSectionInstallerBuilding.GetPath, FMenuSectionInstallerBuilding.Name + 'File' + IntToStr(FMenuSectionInstallerBuilding.Count), '');
      end;
      while FMenuSectionInstallerBuilding.Count > Succ(slFiles.Count) do begin
         FMenuSectionInstallerBuilding.Items[Pred(FMenuSectionInstallerBuilding.Count)].Free;
      end;
      for i := 0 to Pred(slFiles.Count) do begin
         mi := FMenuSectionInstallerBuilding.Items[Succ(i)];
         mi.Caption := slFiles[i];
         mi.Enabled := FileExists(slFiles[i]);
         mi.OnClick := @DoCreateSpecificInstaller;
      end;
   finally
      slFiles.Free;
   end;
end;

procedure TInstallerHelper.DoProjectBuildingFinished(ASender: TObject; ABuildSuccessful: boolean);
begin
   if ABuildSuccessful then begin
      CreateProjectFileInstallers(True);
   end;
end;

procedure TInstallerHelper.DoSourceEditorWindowActivate(Sender: TObject);
var
   edit: TSourceEditorInterface;
   ft: TInstallerType;
begin
   edit := SourceEditorManagerIntf.ActiveEditor;
   if Assigned(edit) then begin
      ft := IdentifyFileType(edit.FileName);
      FMenuItemSourceEditorBuild.Visible := (ft <> itNone);
   end else begin
      FMenuItemSourceEditorBuild.Visible := False;
   end;
end;

class constructor TInstallerHelper.Create;
begin
   FInstance := nil;
end;

class destructor TInstallerHelper.Destroy;
begin
   FInstance.Free;
end;

constructor TInstallerHelper.Create;
begin
   AddHandlers();
   SetLength(FBuildingFinishedEvents, 0);
end;

destructor TInstallerHelper.Destroy;
begin
   SetLength(FBuildingFinishedEvents, 0);
   inherited Destroy;
end;

function TInstallerHelper.AddBuildingFinishedHandler(AHandler: TInstallerBuildingFinishedEvent): integer;
begin
   Result := Length(FBuildingFinishedEvents);
   SetLength(FBuildingFinishedEvents, Succ(Result));
   FBuildingFinishedEvents[Result] := AHandler;
end;

procedure TInstallerHelper.RemoveBuildingFinishedHandler(AHandler: TInstallerBuildingFinishedEvent);
var
   i, iFound: integer;
begin
   iFound := -1;
   for i := Low(FBuildingFinishedEvents) to High(FBuildingFinishedEvents) do begin
      if FBuildingFinishedEvents[i] = AHandler then begin
         iFound := i;
         break;
      end;
   end;
   if iFound > -1 then begin
      if iFound < Pred(Length(FBuildingFinishedEvents)) then begin
         FBuildingFinishedEvents[iFound] := FBuildingFinishedEvents[Pred(Length(FBuildingFinishedEvents))];
      end;
      SetLength(FBuildingFinishedEvents, Pred(Length(FBuildingFinishedEvents)));
   end;
end;

procedure TInstallerHelper.AddHandlers();
begin
   LazarusIDE.AddHandlerOnProjectBuildingFinished(@DoProjectBuildingFinished, True);
   LazarusIDE.AddHandlerOnIDEClose(@DoIDEClose);
   SourceEditorManagerIntf.RegisterChangeEvent(semWindowActivate, @DoSourceEditorWindowActivate);
end;

procedure TInstallerHelper.RemoveHandlers();
begin
   LazarusIDE.RemoveHandlerOnProjectBuildingFinished(@DoProjectBuildingFinished);
   LazarusIDE.RemoveHandlerOnIDEClose(@DoIDEClose);
   SourceEditorManagerIntf.UnregisterChangeEvent(semWindowActivate, @DoSourceEditorWindowActivate);
end;

procedure TInstallerHelper.CreateMainMenuSubMenu();
begin
   RegisterIDEMenuCommand(mnuProject, 'InstallerBuildInstaller', 'Build All Installers', @DoCreateInstallers);
   FMenuSectionInstallerBuilding := RegisterIDESubMenu(mnuProject, 'InstallerBuildSpecificMenu', 'Build Specific Installer', @DoListInstallersToSubMenu);
   RegisterIDEMenuCommand(FMenuSectionInstallerBuilding, 'InstallerBuildInstaller', 'Build All Installers', @DoCreateInstallers);
   FMenuItemSourceEditorBuild := RegisterIDEMenuCommand(SourceEditorMenuRoot, 'InstallerBuildSpecificEditorMenu', 'Build Installer', @DoCreateInstallerForEditorFile);
end;

function TInstallerHelper.BuildInnoSetupInstallerUsingLibrary(AFilename: string; AUpdateVersion: boolean; ANewVersion: string): boolean;
var
   isc: TInstallerHelperInnoSetupCompilerLibrary;
   sView: string;
   sVersionOld: string;
   sLibraryFilename: string;
   b: boolean;
   i: integer;
   //v: TExtToolView;
begin
   Result := False;
   sLibraryFilename := TInstallerOptions(TInstallerOptions.GetInstance).InnoSetup.LibraryFilename;
   sView := Format('Build InnoSetup installer using %s', [AFilename]);
   //v := IDEMessagesWindow.CreateView(sView);
   //v := IDEMessagesWindow.GetView(sView, true);
   if FileExists(sLibraryFilename) then begin
      DumpFileInformation('Using InnoSetup compiler library %s: %s', UTF8Encode(sLibraryFilename), sView);
   end else begin
      AddIDEMessage(mluError, Format('Unable to locate InnoSetup library at %s', [sLibraryFilename]), '', 0, 0, sView);
      Exit;
   end;
   isc := TInstallerHelperInnoSetupCompilerLibrary.Create;
   try
      isc.LibraryFilename := UTF8Encode(sLibraryFilename);
      isc.ScriptFilename := AFilename;
      isc.ViewName := sView;
      if AUpdateVersion then begin
         sVersionOld := isc.VersionNumber;
         if (isc.VersionNumber = ANewVersion) then begin
            AddIDEMessage(mluNote, Format('Installer script version %s is par to the project version.', [sVersionOld]), '', 0, 0, sView);
         end else begin
            isc.VersionNumber := ANewVersion;
            if (isc.VersionNumber = ANewVersion) then begin
               AddIDEMessage(mluImportant, Format('Updated version %s to %s.', [sVersionOld, ANewVersion]), '', 0, 0, sView);
            end else begin
               AddIDEMessage(mluError, Format('Failed to update version from %s to %s!', [sVersionOld, ANewVersion]), '', 0, 0, sView);
            end;
         end;
      end;
      b := isc.Build();
      if b then begin
         AddIDEMessage(mluImportant, 'Created InnoSetup installer.', '', 0, 0, sView);
         i := CallBuildingFinishedHandlers(isc.ScriptFilename, isc.OutputFilename, sView);
         AddIDEMessage(mluNote, Format('Called %d post-build handlers.', [i]), '', 0, 0, sView);
      end else begin
         AddIDEMessage(mluError, 'Failed creating InnoSetup installer.', '', 0, 0, sView);
      end;
   finally
      isc.Free;
   end;
end;

function TInstallerHelper.BuildNSISInstaller(AFilename: string): boolean;
var
   sView: string;
   ibn: TInstallerHelperNSISCompilerTool;
   sExecutable: string;
   b: boolean;
   i: integer;
begin
   Result := False;
   sView := Format('Build NSIS installer using %s', [AFilename]);
   sExecutable := TInstallerOptions(TInstallerOptions.GetInstance).NSIS.ExecutableFilename;
   if FileExists(sExecutable) then begin
      DumpFileInformation('Using NSIS compiler executable %s: %s', UTF8Encode(sExecutable), sView);
   end else begin
      AddIDEMessage(mluError, Format('Unable to locate NSIS compiler executable at %s', [sExecutable]), '', 0, 0, sView);
      Exit;
   end;
   ibn := TInstallerHelperNSISCompilerTool.Create;
   try
      ibn.ExexutableFilename := UTF8Encode(sExecutable);
      ibn.ScriptFilename := AFilename;
      ibn.ViewName := sView;
      b := ibn.Build();
      if b then begin
         AddIDEMessage(mluImportant, 'Created NSIS installer.', '', 0, 0, sView);
         i := CallBuildingFinishedHandlers(ibn.ScriptFilename, ibn.OutputFilename, sView);
         AddIDEMessage(mluNote, Format('Called %d post-build handlers.', [i]), '', 0, 0, sView);
      end else begin
         AddIDEMessage(mluError, 'Failed creating NSIS installer.', '', 0, 0, sView);
      end;
   finally
      ibn.Free;
   end;
end;

function TInstallerHelper.BuildWiXInstaller(AFilename: string): boolean;
var
   sView: string;
   iwt: TInstallerHelperWiXTool;
   sExecutable: string;
   b: boolean;
begin
   Result := False;
   sView := Format('Build WiX Toolkit installer using %s', [AFilename]);
   sExecutable := TInstallerOptions(TInstallerOptions.GetInstance).WiX.ExecutableFilename;
   if FileExists(sExecutable) then begin
      DumpFileInformation('Using WiX Toolkit compiler executable %s: %s', UTF8Encode(sExecutable), sView);
   end else begin
      AddIDEMessage(mluError, Format('Unable to locate WiX Toolkit compiler executable at %s', [sExecutable]), '', 0, 0, sView);
      Exit;
   end;
   iwt := TInstallerHelperWiXTool.Create;
   try
      iwt.ExexutableFilename := UTF8Encode(sExecutable);
      iwt.ScriptFilename := AFilename;
      iwt.ViewName := sView;
      b := iwt.Build();
      if b then begin
         AddIDEMessage(mluImportant, 'Created WiX Toolkit installer.', '', 0, 0, sView);
      end else begin
         AddIDEMessage(mluError, 'Failed creating WiX Toolkit installer.', '', 0, 0, sView);
      end;
   finally
      iwt.Free;
   end;
end;

function TInstallerHelper.BuildInstaller(AFilename: string): boolean;
var
   it: TInstallerType;
begin
   Result := False;
   it := IdentifyFileType(AFilename);
   case it of
      itNSIS: Result := BuildNSISInstaller(AFilename);
      itInnoSetup: Result := BuildInnoSetupInstallerUsingLibrary(AFilename);
      itWiX: Result := BuildWiXInstaller(AFilename);
      else begin
         // no support for other file types currently, add new installer engines here
      end;
   end;
end;

end.
