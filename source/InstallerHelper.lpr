{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-01  --  ---  Renamed from ram InstallerHelper to InstallerHelper
// 2017-06-01  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program InstallerHelper;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
   cthreads, {$ENDIF} {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms, InstallerHelper.Form.Main, InstallerCodeSigningHelper.Menu,
   TestMenuItemVisibilityMain, InstallerHelper.InnoSetup.Frame,
   InstallerHelper.NSIS.Frame, InstallerHelper.WiX.Frame,
   InstallerHelper.Options;

{$R *.res}

begin
   RequireDerivedFormResource := True;
   Application.Initialize;
   Application.CreateForm(TForm1, Form1);
   Application.Run;
end.

