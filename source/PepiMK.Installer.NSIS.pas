{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer building support for NSIS.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit PepiMK.Installer.NSIS;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   PepiMK.Installer.Base;

type

   { TInstallerBuilderNSIS }

   TInstallerBuilderNSIS = class(TCustomInstallerBuilder)
   protected
      procedure ConstructBuildParameters(AAdder: TConstructParametersProc); override;
   end;

function FindNSISExecutableLocation(): string;

implementation

uses
   registry;

function DetectNSISLocation(out APath: string): boolean;
var
   reg: TRegistry;
begin
   Result := False;
   try
      reg := TRegistry.Create;
      try
         reg.RootKey := HKEY_LOCAL_MACHINE;
         if reg.OpenKey('\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\NSIS', False) then begin
            try
               APath := reg.ReadString('InstallLocation');
               Result := True;
            finally
               reg.CloseKey();
            end;
         end;
      finally
         reg.Free;
      end;
   except
      Result := False;
   end;
end;

function FindNSISExecutableLocation: string;
var
   s: string;
begin
   if DetectNSISLocation(s) then begin
      Result := IncludeTrailingPathDelimiter(s) + 'makensis.exe';
      Exit;
   end;
   Result := 'c:\Program Files (x86)\NSIS\makensis.exe';
end;

{ TInstallerBuilderNSIS }

procedure TInstallerBuilderNSIS.ConstructBuildParameters(AAdder: TConstructParametersProc);

begin
   AAdder(ScriptFilename);
end;

end.
