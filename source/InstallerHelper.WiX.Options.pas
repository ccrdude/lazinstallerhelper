{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer options for WiX.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.WiX.Options;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   LazIDEIntf,
   //IDEOptionsIntf,
   LazConfigStorage,
   IniFiles;

type

   { TInstallerWiXOptions }

   TInstallerWiXOptions = class(TPersistent)
   private
      FAutoCreate: boolean;
      FAutoSign: boolean;
      FExecutableFilename: string;
      FConfigFilename: string;
   public
      constructor Create;
      destructor Destroy; override;
      procedure LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASectionName: string);
      procedure LoadFromStorage(AStorage: TConfigStorage);
      procedure SaveToStorage(AStorage: TConfigStorage);
      procedure Assign(Source: TPersistent); override;
      property AutoCreate: boolean read FAutoCreate write FAutoCreate;
      property AutoSign: boolean read FAutoSign write FAutoSign;
      property ExecutableFilename: string read FExecutableFilename write FExecutableFilename;
   end;

implementation

uses
   //IDEMsgIntf,
   IDEExternToolIntf,
   PepiMK.Installer.WiX;

{ TInstallerWiXOptions }

procedure TInstallerWiXOptions.LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
begin
   FAutoCreate := AIni.ReadBool(ASectionName, 'AutoCreate', False);
   FAutoSign := AIni.ReadBool(ASectionName, 'AutoSign', False);
   FExecutableFilename := AIni.ReadString(ASectionName, 'ExecutableFilename', UTF8Encode(FExecutableFilename));
end;

procedure TInstallerWiXOptions.SaveToIni(AIni: TCustomIniFile; ASectionName: string);
begin
   AIni.WriteBool(ASectionName, 'AutoCreate', FAutoCreate);
   AIni.WriteBool(ASectionName, 'AutoSign', False);
   AIni.WriteString(ASectionName, 'ExecutableFilename', UTF8Encode(FExecutableFilename));
end;

procedure TInstallerWiXOptions.LoadFromStorage(AStorage: TConfigStorage);
begin
   FExecutableFilename := AStorage.GetValue('WiX/ExecutableFilename', '');
   FAutoCreate := AStorage.GetValue('WiX/AutoCreate', false);
   FAutoSign := AStorage.GetValue('WiX/AutoSign', false);
end;

procedure TInstallerWiXOptions.SaveToStorage(AStorage: TConfigStorage);
begin
   AStorage.SetDeleteValue('WiX/ConfigVersion', 1, 0);
   AStorage.SetDeleteValue('WiX/ExecutableFilename', FExecutableFilename, '');
   AStorage.SetDeleteValue('WiX/AutoCreate', FAutoCreate, false);
   AStorage.SetDeleteValue('WiX/AutoSign', FAutoSign, false);
end;

constructor TInstallerWiXOptions.Create;
begin
   inherited Create;
   FAutoCreate := False;
   FAutoSign := False;
   FExecutableFilename := FindWiXExecutableLocation;
end;

destructor TInstallerWiXOptions.Destroy;
begin
   inherited Destroy;
end;

procedure TInstallerWiXOptions.Assign(Source: TPersistent);
begin
   if Source is TInstallerWiXOptions then begin
      inherited Assign(Source);
      Self.ExecutableFilename := TInstallerWiXOptions(Source).ExecutableFilename;
      Self.AutoSign := TInstallerWiXOptions(Source).AutoSign;
      Self.AutoCreate := TInstallerWiXOptions(Source).AutoCreate;
   end else begin
      inherited Assign(Source);
   end;
end;

end.
