{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-06  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.WiX.Frame;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   EditBtn,
   StdCtrls,
   IDEOptEditorIntf,
   IDEOptionsIntf,
   InstallerHelper.Options,
   InstallerHelper.WiX.Options;

type

   { TFrameInstallerOptionsWiX }

   TFrameInstallerOptionsWiX = class(TAbstractIDEOptionsEditor)
      cbAutoBuild: TCheckBox;
      cbAutoSign: TCheckBox;
      editExecutable: TFileNameEdit;
      groupAutomation: TGroupBox;
      groupExecutable: TGroupBox;
   public
      { public declarations }
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings({%H-}AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings({%H-}AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

{$R *.lfm}

{ TFrameInstallerOptionsWiX }

function TFrameInstallerOptionsWiX.GetTitle: string;
begin
   Result := 'WiX Toolkit';
end;

procedure TFrameInstallerOptionsWiX.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   cbAutoSign.Enabled := Assigned(FindClass('TInstallerCodeSigningHelper'));
end;

procedure TFrameInstallerOptionsWiX.ReadSettings(AOptions: TAbstractIDEOptions);
var
   o: TInstallerWiXOptions;
begin
   o := TInstallerOptions(TInstallerOptions.GetInstance).WiX;
   cbAutoBuild.Checked := o.AutoCreate;
   cbAutoSign.Checked := o.AutoSign;
   editExecutable.Text := UTF8Encode(o.ExecutableFilename);
end;

procedure TFrameInstallerOptionsWiX.WriteSettings(AOptions: TAbstractIDEOptions);
var
   o: TInstallerWiXOptions;
begin
   o := TInstallerOptions(TInstallerOptions.GetInstance).WiX;
   o.AutoCreate := cbAutoBuild.Checked;
   o.AutoSign := cbAutoSign.Checked;
   o.ExecutableFilename := editExecutable.Text;
end;

class function TFrameInstallerOptionsWiX.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TInstallerOptions;
end;

end.
