{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit TestMenuItemVisibilityPackage;

{$warn 5023 off : no warning about unused units}
interface

uses
  TestMenuItemVisibilityMain, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('TestMenuItemVisibilityMain', 
    @TestMenuItemVisibilityMain.Register);
end;

initialization
  RegisterPackage('TestMenuItemVisibilityPackage', @Register);
end.
