{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer building support for WiX Toolkit.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit PepiMK.Installer.WiX;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   PepiMK.Installer.Base;

type

   { TInstallerBuilderWiX }

   TInstallerBuilderWiX = class(TCustomInstallerBuilder)
   protected
      procedure ConstructBuildParameters(AAdder: TConstructParametersProc); override;
      procedure ConstructLightParameters(AAdder: TConstructParametersProc);
   public
      function BuildInstaller(const AScriptFilename: WideString): boolean; override;
   end;

function FindWiXExecutableLocation(): string;

implementation

uses
   registry;

function DetectWiXLocation(out APath: string): boolean;
var
   reg: TRegistry;
   sl: TStringList;
begin
   Result := False;
   try
      reg := TRegistry.Create;
      sl := TStringList.Create;
      try
         reg.RootKey := HKEY_LOCAL_MACHINE;
         if reg.OpenKey('\SOFTWARE\Microsoft\Windows Installer XML\3.x', False) then begin
            try
               reg.GetValueNames(sl);
               sl.Sort;
               if sl.Count > 0 then begin
                  APath := reg.ReadString(sl[Pred(sl.Count)]);
                  Result := True;
               end;
            finally
               reg.CloseKey();
            end;
         end;
      finally
         reg.Free;
         sl.Free;
      end;
   except
      Result := False;
   end;
end;

function FindWiXExecutableLocation: string;
var
   s: string;
begin
   if DetectWiXLocation(s) then begin
      Result := IncludeTrailingPathDelimiter(s) + 'bin\candle.exe';
      Exit;
   end;
   Result := 'c:\Program Files (x86)\WiX Toolset v3.11\bin\candle.exe';
end;

{ TInstallerBuilderWiX }

procedure TInstallerBuilderWiX.ConstructBuildParameters(AAdder: TConstructParametersProc);
begin
   AAdder(ScriptFilename);
end;

procedure TInstallerBuilderWiX.ConstructLightParameters(AAdder: TConstructParametersProc);
var
   sFilename: WideString;
begin
   sFilename := Copy(ScriptFilename, 1, Length(ScriptFilename) - Length(ExtractFileExt(ScriptFilename))) + '.wixobj';
   AAdder(sFilename);
end;

function TInstallerBuilderWiX.BuildInstaller(const AScriptFilename: WideString): boolean;
var
   slOutput: TStringList;
   slErrors: TStringList;
begin
   OutputFilename := Copy(ScriptFilename, 1, Length(ScriptFilename) - Length(ExtractFileExt(ScriptFilename))) + '.msi';
   slOutput := TStringList.Create;
   slErrors := TStringList.Create;
   try
      BuilderFilename := ExtractFilePath(BuilderFilename) + 'candle.exe';
      Result := inherited BuildInstaller(AScriptFilename);
      slOutput.Assign(Outcome.Output);
      slErrors.Assign(Outcome.Errors);
      if Result then begin
         BuilderFilename := ExtractFilePath(BuilderFilename) + 'light.exe';
         Result := Execute(@ConstructLightParameters);
         slOutput.AddStrings(Outcome.Output);
         slErrors.AddStrings(Outcome.Errors);
      end;
   finally
      Outcome.Output.Assign(slOutput);
      Outcome.Errors.Assign(slErrors);
      slOutput.Free;
      slErrors.Free;
   end;
end;

end.
