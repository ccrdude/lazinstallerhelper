{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-06  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.NSIS.Frame;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   EditBtn,
   StdCtrls,
   IDEOptEditorIntf,
   IDEOptionsIntf,
   InstallerHelper.Options,
   InstallerHelper.NSIS.Options;

type

   { TFrameInstallerOptionsNSIS }

   TFrameInstallerOptionsNSIS = class(TAbstractIDEOptionsEditor)
      cbAutoBuild: TCheckBox;
      cbAutoSign: TCheckBox;
      editExecutable: TFileNameEdit;
      groupAutomation: TGroupBox;
      groupExecutable: TGroupBox;
   public
      { public declarations }
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings({%H-}AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings({%H-}AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

{$R *.lfm}

{ TFrameInstallerOptionsNSIS }

function TFrameInstallerOptionsNSIS.GetTitle: string;
begin
   Result := 'NSIS';
end;

procedure TFrameInstallerOptionsNSIS.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   cbAutoSign.Enabled := Assigned(FindClass('TInstallerCodeSigningHelper'));
end;

procedure TFrameInstallerOptionsNSIS.ReadSettings(AOptions: TAbstractIDEOptions);
var
   o: TInstallerNSISOptions;
begin
   o := TInstallerOptions(TInstallerOptions.GetInstance).NSIS;
   cbAutoBuild.Checked := o.AutoCreate;
   cbAutoSign.Checked := o.AutoSign;
   editExecutable.Text := UTF8Encode(o.ExecutableFilename);
end;

procedure TFrameInstallerOptionsNSIS.WriteSettings(AOptions: TAbstractIDEOptions);
var
   o: TInstallerNSISOptions;
begin
   o := TInstallerOptions(TInstallerOptions.GetInstance).NSIS;
   o.AutoCreate := cbAutoBuild.Checked;
   o.AutoSign := cbAutoSign.Checked;
   o.ExecutableFilename := editExecutable.Text;
end;

class function TFrameInstallerOptionsNSIS.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TInstallerOptions;
end;

end.
