{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer options for InnoSetup.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.InnoSetup.Options;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   LazIDEIntf,
   //IDEOptionsIntf,
   LazConfigStorage,
   IniFiles;

type

   { TInstallerInnoSetupOptions }

   TInstallerInnoSetupOptions = class(TPersistent)
   private
      FAutoCreate: boolean;
      FAutoSign: boolean;
      FLibraryFilename: string;
      FConfigFilename: string;
   public
      constructor Create;
      destructor Destroy; override;
      procedure LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASectionName: string);
      procedure LoadFromStorage(AStorage: TConfigStorage);
      procedure SaveToStorage(AStorage: TConfigStorage);
      procedure Assign(Source: TPersistent); override;
      property AutoCreate: boolean read FAutoCreate write FAutoCreate;
      property AutoSign: boolean read FAutoSign write FAutoSign;
      property LibraryFilename: string read FLibraryFilename write FLibraryFilename;
   end;

implementation

uses
   //IDEMsgIntf,
   IDEExternToolIntf,
   PepiMK.Installer.InnoSetup;

{ TInstallerInnoSetupOptions }

procedure TInstallerInnoSetupOptions.LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
begin
   FAutoCreate := AIni.ReadBool(ASectionName, 'AutoCreate', False);
   FAutoSign := AIni.ReadBool(ASectionName, 'AutoSign', False);
   FLibraryFilename := AIni.ReadString(ASectionName, 'LibraryFilename', UTF8Encode(FLibraryFilename));
end;

procedure TInstallerInnoSetupOptions.SaveToIni(AIni: TCustomIniFile; ASectionName: string);
begin
   AIni.WriteBool(ASectionName, 'AutoCreate', FAutoCreate);
   AIni.WriteBool(ASectionName, 'AutoSign', False);
   AIni.WriteString(ASectionName, 'LibraryFilename', FLibraryFilename);
end;

procedure TInstallerInnoSetupOptions.LoadFromStorage(AStorage: TConfigStorage);
begin
   FLibraryFilename := AStorage.GetValue('InnoSetup/LibraryFilename', '');
   FAutoCreate := AStorage.GetValue('InnoSetup/AutoCreate', false);
   FAutoSign := AStorage.GetValue('InnoSetup/AutoSign', false);
end;

procedure TInstallerInnoSetupOptions.SaveToStorage(AStorage: TConfigStorage);
begin
   AStorage.SetDeleteValue('InnoSetup/ConfigVersion', 1, 0);
   AStorage.SetDeleteValue('InnoSetup/LibraryFilename', FLibraryFilename, '');
   AStorage.SetDeleteValue('InnoSetup/AutoCreate', FAutoCreate, false);
   AStorage.SetDeleteValue('InnoSetup/AutoSign', FAutoSign, false);
end;

constructor TInstallerInnoSetupOptions.Create;
begin
   inherited Create;
   FAutoCreate := False;
   FAutoSign := False;
   FLibraryFilename := FindInnoSetupLibraryLocation();
end;

destructor TInstallerInnoSetupOptions.Destroy;
begin
   inherited Destroy;
end;

procedure TInstallerInnoSetupOptions.Assign(Source: TPersistent);
begin
   if Source is TInstallerInnoSetupOptions then begin
      inherited Assign(Source);
      Self.LibraryFilename := TInstallerInnoSetupOptions(Source).LibraryFilename;
      Self.AutoSign := TInstallerInnoSetupOptions(Source).AutoSign;
      Self.AutoCreate := TInstallerInnoSetupOptions(Source).AutoCreate;
   end else begin
      inherited Assign(Source);
   end;
end;

end.
