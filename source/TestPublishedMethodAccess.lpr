{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Tests TMethod based access to class functions.)
   @see(http://forum.lazarus.freepascal.org/index.php/topic,37075.msg248068.html#msg248068)

   @preformatted(
// *****************************************************************************
// Credits go to SkyKhan at the Lazarus forums for demonstrating this to me.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-01  pk  ---  [CCD] Created test program.
// *****************************************************************************
// Example output:
// ---------------------------------------
// 014D4AE0: Hello World
// 014D4AE0: Hello Welt
// Press ENTER to finish.
// ---------------------------------------
// This demonstrates that the Test method of the same object was called.
// *****************************************************************************
   )
}

program TestPublishedMethodAccess;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses
   SysUtils,
   Classes,
   TypInfo;

const
   STestInterface = '{74444ED6-B443-4C7E-A4C3-FD8809BA57E7}';

type
   ITestInterface = interface
      [STestInterface]
      procedure Test(AText: string);
   end;

type
   TTest = procedure(AText: string) of object;
   TInstance = function: TPersistent of object;

   { TTestClass }

   TTestClass = class(TInterfacedPersistent, ITestInterface)
   private
      class var FInstance: TTestClass;
   public
      class constructor Create;
      class destructor Destroy;
   published
      procedure Test(AText: string);
      class function Instance: TTestClass;
   end;

   { TTestClass }

   class constructor TTestClass.Create;
   begin
      FInstance := nil;
      RegisterClass(TTestClass);
   end;

   class destructor TTestClass.Destroy;
   begin
      FInstance.Free;
   end;

   class function TTestClass.Instance: TTestClass;
   begin
      if not Assigned(FInstance) then begin
         FInstance := TTestClass.Create;
      end;
      Result := FInstance;
   end;

   procedure TTestClass.Test(AText: string);
   begin
      WriteLn(Format('%p: Hello %s', [Pointer(Self), AText]));
   end;

   function FindPersistentObjectUsingClassMethod(AClassName: string; AInstanceMethodName: string = 'Instance'): TPersistent;
   var
      c: TPersistentClass;
      m: TMethod;
   begin
      Result := nil;
      c := FindClass(AClassName);
      if not Assigned(c) then begin
         Exit;
      end;
      m.Code := c.MethodAddress(AInstanceMethodName);
      if Assigned(m.Code) then begin
         m.Data := nil;
         Result := TInstance(m)();
      end;
   end;

   function FindNamedInterfaceForPersistentObjectUsingClassMethod(AClassName, AInterfaceGUIDString: string; AInstanceMethodName: string = 'Instance'): IUnknown;
   var
      o: TPersistent;
   begin
      Result := nil;
      o := FindPersistentObjectUsingClassMethod(AClassName, AInstanceMethodName);
      if not Assigned(o) then begin
         Exit;
      end;
      o.GetInterface(AInterfaceGUIDString, Result);
   end;

   procedure TestPublishedMethodAccess;
   var
      c: TPersistentClass;
      o: TPersistent;
      m: TMethod;
   begin
      o := nil;
      c := FindClass('TTestClass');
      if not Assigned(c) then begin
         Exit;
      end;
      m.Code := c.MethodAddress('Instance');
      if Assigned(m.Code) then begin
         m.Data := nil;
         o := TInstance(m)();
      end;
      if not Assigned(o) then begin
         Exit;
      end;
      m.Code := c.MethodAddress('Test');
      if Assigned(m.Code) then begin
         m.Data := o;
         TTest(m)('Welt');
      end;
   end;

procedure TestPublishedMethodAccess2;
var
   o: TPersistent;
   i: ITestInterface;
begin
   o := FindPersistentObjectUsingClassMethod('TTestClass');
   if not Assigned(o) then begin
      Exit;
   end;
   if o.GetInterface(STestInterface, i) then begin
      i.Test('Patrick');
   end;
end;

procedure TestPublishedMethodAccess3;
var
   i: ITestInterface;
begin
   i := ITestInterface(FindNamedInterfaceForPersistentObjectUsingClassMethod('TTestClass', STestInterface, 'Instance'));
   if Assigned(i) then begin
      i.Test('Patrick Michael');
   end;
end;

   procedure TestPublishedMethodAccess4;
   var
      c: TPersistentClass;
      i: ITestInterface;
      p: PPropInfo;
   begin
      c := FindClass('TTestClass');
      p := GetPropInfo(c, 'Instance');
      if Assigned(p) then begin
         WriteLn(p^.Name);
      end;
      // fail, object not known!
      //if c.GetInterfaceByStr(STestInterface, i) then begin
      //   i.Test('Patrick');
      //end;
   end;

begin
   try
      TTestClass.Instance.Test('World');
      TestPublishedMethodAccess;
      TestPublishedMethodAccess2;
      TestPublishedMethodAccess3;
      TestPublishedMethodAccess4;
   finally
      WriteLn('Press ENTER to finish.');
      ReadLn;
   end;
end.
