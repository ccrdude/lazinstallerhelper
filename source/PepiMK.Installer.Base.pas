{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer building support base class.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit PepiMK.Installer.Base;

{$IFDEF FPC}
{$MODE objfpc}{$H+}
{$modeswitch nestedprocvars}
{$ENDIF FPC}

interface

uses
   Classes,
   {$IFDEF FPC}
   Process,
   Pipes,
   {$ELSE FPC}
   DosCommand,
   {$ENDIF FPC}
   Windows, // for FillChar, ...
   SysUtils;

type
   TConstructParametersProc = procedure(AText: WideString) is nested;
   TConstructMethod = procedure(AAdder: TConstructParametersProc) of object;

   { TCustomInstallerBuilderResult }

   TCustomInstallerBuilderResult = class
   private
      FCommandLine: string;
      FExitCode: integer;
      FErrors: TStringList;
      FOutput: TStringList;
   public
      constructor Create;
      destructor Destroy; override;
      property CommandLine: string read FCommandLine;
      property ExitCode: integer read FExitCode;
      property Errors: TStringList read FErrors;
      property Output: TStringList read FOutput;
   end;

   TCustomInstallerBuilder = class
      abstract
   private
      FBuilderFilename: WideString;
      FOutputFilename: WideString;
      FOutcome: TCustomInstallerBuilderResult;
      FScriptFilename: WideString;
   protected
      procedure ConstructBuildParameters(AAdder: TConstructParametersProc); virtual; abstract;
      function Execute(AConstructor: TConstructMethod): boolean;
      {$IFDEF FPC}
      function ExecuteFreePascal(AConstructor: TConstructMethod): boolean;
      {$ELSE FPC}
      function ExecuteDosCommand(AConstructor: TConstructMethod): boolean;
      procedure DoDosCommandNewExecLine(ASender: TObject; ANewLine: string; AOutputType: TOutputType);
      {$ENDIF FPC}
   public
      constructor Create; virtual;
      destructor Destroy; override;
      function BuildInstaller(const AScriptFilename: WideString): boolean; virtual;
      property Outcome: TCustomInstallerBuilderResult read FOutcome;
      property BuilderFilename: WideString read FBuilderFilename write FBuilderFilename;
      property ScriptFilename: WideString read FScriptFilename;
      property OutputFilename: WideString read FOutputFilename write FOutputFilename;
   end;

implementation

{ TCustomInstallerBuilder }

function TCustomInstallerBuilder.Execute(AConstructor: TConstructMethod): boolean;
begin
   {$IFDEF FPC}
   Result := ExecuteFreePascal(AConstructor);
   {$ELSE FPC}
   Result := ExecuteDosCommand(AConstructor);
   {$ENDIF FPC}
end;

function TCustomInstallerBuilder.ExecuteFreePascal(AConstructor: TConstructMethod): boolean;
var
   p: TProcess;

   procedure Adder(AText: WideString);
   begin
      p.Parameters.Add(UTF8Encode(AText));
   end;

   procedure ProcessInput(AProcess: TProcess);
   var
      ssOutput: TStringStream;
      ssError: TStringStream;
      iLinesPushedError: integer;
      iLinesPushedOutput: integer;

      procedure TriggerLines(ALines: TStrings; AWithoutLastNLines: integer; var ALineCounter: integer);
      var
         i: integer;
      begin
         if (ALines.Count - AWithoutLastNLines > ALineCounter) then begin
            for i := ALineCounter to Pred(ALines.Count) - AWithoutLastNLines do begin
               // TODO : trigger event
            end;
            ALineCounter := Pred(ALines.Count);
         end;
      end;

      function ProcessPipeStream(APipeStream: TInputPipeStream; AString: TStringStream; AOutput: TStringList; var ALineCounter: integer): integer;
      const
         BUF_SIZE = 2048; // Buffer size for reading the output in chunks
      var
         iRead: integer;
         Buffer: array[1..BUF_SIZE] of byte;
      begin
         ZeroMemory(@Buffer[1], BUF_SIZE); // 2021-09-01  pk  fixing warnings
         iRead := APipeStream.Read(Buffer, BUF_SIZE);
         if (iRead > 0) then begin
            AString.Write(Buffer, iRead);
            AString.Seek(0, soFromBeginning);
            AOutput.Clear;
            AOutput.LoadFromStream(AString);
            TriggerLines(AOutput, 1, ALineCounter);
         end;
         Result := iRead;
      end;

   var
      iRead: integer;
   begin
      iLinesPushedOutput := 0;
      iLinesPushedError := 0;
      ssOutput := TStringStream.Create('');
      ssError := TStringStream.Create('');
      try
         repeat
            iRead := ProcessPipeStream(AProcess.Output, ssOutput, Outcome.Output, iLinesPushedOutput);
            if p.Stderr.NumBytesAvailable > 0 then begin
               iRead += ProcessPipeStream(AProcess.Stderr, ssError, Outcome.Errors, iLinesPushedError);
            end;
         until (iRead = 0) and (not p.Running);
         TriggerLines(Outcome.Output, 0, iLinesPushedOutput);
         TriggerLines(Outcome.Errors, 0, iLinesPushedError);
      finally
         ssOutput.Free;
         ssError.Free;
      end;
   end;

begin
   try
      p := TProcess.Create(nil);
      try
         p.Options := p.Options + [poUsePipes, poNoConsole]; // , poWaitOnExit , poStderrToOutPut
         p.Executable := UTF8Encode(BuilderFilename);
         p.CurrentDirectory := UTF8Encode(ExtractFilePath(ScriptFilename));
         p.Parameters.StrictDelimiter := False;
         p.Parameters.Delimiter := ' ';
         AConstructor(@Adder);
         {$IFDEF Darwin}
         p.Options := p.Options + [poWaitOnExit];
         {$ENDIF Darwin}
         p.Execute;
         ProcessInput(p);
         Result := (p.ExitCode = 0);
         FOutcome.FCommandLine := '"' + p.Executable + '" ' + p.Parameters.DelimitedText;
         FOutcome.FExitCode := p.ExitCode;
      finally
         p.Free;
      end;
   except
      on E: Exception do begin
         Result := False;
         FOutcome.Errors.Add(E.Message);
      end;
   end;
end;

constructor TCustomInstallerBuilder.Create;
begin
   FOutcome := TCustomInstallerBuilderResult.Create;
end;

destructor TCustomInstallerBuilder.Destroy;
begin
   FOutcome.Free;
   inherited Destroy;
end;

function TCustomInstallerBuilder.BuildInstaller(const AScriptFilename: WideString): boolean;
begin
   FScriptFilename := AScriptFilename;
   Outcome.Output.Clear;
   Outcome.Errors.Clear;
   Result := Execute(@ConstructBuildParameters);
end;

{ TCustomInstallerBuilderResult }

constructor TCustomInstallerBuilderResult.Create;
begin
   FErrors := TStringList.Create;
   FOutput := TStringList.Create;
end;

destructor TCustomInstallerBuilderResult.Destroy;
begin
   FErrors.Free;
   FOutput.Free;
   inherited Destroy;
end;

end.
