{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer support for WiX Toolkit.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.WiX.CommandLine;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   ProjectIntf,
   LazIDEIntf,
   IDEMsgIntf,
   IDEExternToolIntf,
   PepiMK.Installer.Base,
   PepiMK.Installer.WiX;

type
   TOnWiXMessageEvent = procedure(AMsg: string; ALineNumber: integer = 0) of object;

   { TInstallerHelperWiXTool }

   TInstallerHelperWiXTool = class
      function GetOutcome: TCustomInstallerBuilderResult;
   private
      FExexutableFilename: string;
      FOnMessage: TOnWiXMessageEvent;
      FOutputFilename: string;
      FScriptFilename: string;
      FViewName: string;
      FBuilder: TInstallerBuilderWiX;
   protected
      function DoMessage(TheUrgency: TMessageLineUrgency; AMsg: string; AFilename: string = ''; ALineNumber: integer = 0): TMessageLine;
   public
      constructor Create;
      destructor Destroy; override;
      function Build: boolean;
      property ExexutableFilename: string read FExexutableFilename write FExexutableFilename;
      property ScriptFilename: string read FScriptFilename write FScriptFilename;
      property ViewName: string read FViewName write FViewName;
      property OutputFilename: string read FOutputFilename;
      property OnMessage: TOnWiXMessageEvent read FOnMessage write FOnMessage;
      property Outcome: TCustomInstallerBuilderResult read GetOutcome;
   end;

implementation

uses
   Regexpr;

{ TInstallerHelperWiXTool }

function TInstallerHelperWiXTool.GetOutcome: TCustomInstallerBuilderResult;
begin
  Result := FBuilder.Outcome;
end;

function TInstallerHelperWiXTool.DoMessage(TheUrgency: TMessageLineUrgency;
   AMsg: string; AFilename: string; ALineNumber: integer): TMessageLine;
begin
  if Length(AFilename) = 0 then begin
     AFilename := FScriptFilename;
  end;
  Result := AddIDEMessage(TheUrgency, AMsg, AFilename, ALineNumber, 0, FViewName);
  if Assigned(FOnMessage) then begin
     FOnMessage(AMsg, ALineNumber);
  end;
end;

constructor TInstallerHelperWiXTool.Create;
begin
  FBuilder := TInstallerBuilderWiX.Create;
  FExexutableFilename := FindWiXExecutableLocation();
end;

destructor TInstallerHelperWiXTool.Destroy;
begin
  FBuilder.Free;
   inherited Destroy;
end;

function TInstallerHelperWiXTool.Build: boolean;
var
   i: integer;
   r: TRegExpr;
begin
   FBuilder.BuilderFilename := UTF8Decode(FExexutableFilename);
   Result := FBuilder.BuildInstaller(UTF8Decode(ScriptFilename));
   r := TRegExpr.Create('Output: "([^\"]*)"');
   try
      for i := 0 to Pred(FBuilder.Outcome.Output.Count) do begin
         DoMessage(mluDebug, FBuilder.Outcome.Output[i]);
         //if r.Exec(FBuilder.Outcome.Output[i]) then begin
         //   FOutputFilename := r.Match[1];
         //end;
      end;
   finally
      r.Free;
   end;
   for i := 0 to Pred(FBuilder.Outcome.Errors.Count) do begin
      DoMessage(mluError, FBuilder.Outcome.Errors[i]);
   end;
   if Result then begin
      DoMessage(mluImportant, 'Installer build completed.');
   end else begin
      DoMessage(mluError, 'Build failed.');
   end;
end;

end.
