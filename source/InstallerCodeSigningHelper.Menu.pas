{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Main class of meta package linking CodeSigning Helper with Installer Helper.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-01  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerCodeSigningHelper.Menu;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   ProjectIntf,
   LazIDEIntf,
   IDEExternToolIntf;

type

   { TInstallerCodeSigningHelper }

   TInstallerCodeSigningHelper = class(TInterfacedPersistent)
   private
      class var FInstance: TInstallerCodeSigningHelper;
   private
      procedure AddHandlers();
      procedure RemoveHandlers();
      procedure DoInstallerBuildingFinished({%H-}AScriptFilename, AOutputFilename, AViewName: string);
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: TInstallerCodeSigningHelper;
   public
      destructor Destroy; override;
   end;

procedure Register;

implementation

uses
   InstallerHelper.Menu,
   CodeSigningHelper.Menu;

procedure Register;
begin
   TInstallerCodeSigningHelper.Instance.AddHandlers();
end;

{ TInstallerCodeSigningHelper }

procedure TInstallerCodeSigningHelper.AddHandlers;
begin
   TInstallerHelper.Instance.AddBuildingFinishedHandler(@DoInstallerBuildingFinished);
end;

procedure TInstallerCodeSigningHelper.RemoveHandlers;
begin
   TInstallerHelper.Instance.RemoveBuildingFinishedHandler(@DoInstallerBuildingFinished);
end;

procedure TInstallerCodeSigningHelper.DoInstallerBuildingFinished(AScriptFilename, AOutputFilename, AViewName: string);
begin
   TCodeSigningHelper.Instance.CertificateSignExecutable(AOutputFilename, '', AViewName);
end;

class constructor TInstallerCodeSigningHelper.Create;
begin
   FInstance := nil;
end;

class destructor TInstallerCodeSigningHelper.Destroy;
begin
   FInstance.Free;
end;

class function TInstallerCodeSigningHelper.Instance: TInstallerCodeSigningHelper;
begin
   if not Assigned(FInstance) then begin
      FInstance := TInstallerCodeSigningHelper.Create;
   end;
   Result := FInstance;
end;

destructor TInstallerCodeSigningHelper.Destroy;
begin
   RemoveHandlers();
   inherited Destroy;
end;

initialization

   RegisterClass(TInstallerCodeSigningHelper);

end.
