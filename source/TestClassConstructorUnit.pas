unit TestClassConstructorUnit;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils;

type

   { TTestClass }

   TTestClass = class
   private
      class var FInstance: TTestClass;
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: TTestClass;
   public
      constructor Create;
      destructor Destroy; override;
      procedure Test;
   end;

implementation

{ TTestClass }

class constructor TTestClass.Create;
begin
   WriteLn('class constructor TTestClass.Create;');
   FInstance := nil;
end;

class destructor TTestClass.Destroy;
begin
   WriteLn('class destructor TTestClass.Destroy;');
   FInstance.Free;
end;

class function TTestClass.Instance: TTestClass;
begin
   if not Assigned(FInstance) then begin
      FInstance := TTestClass.Create;
   end;
   Result := FInstance;
end;

constructor TTestClass.Create;
begin
   WriteLn('constructor TTestClass.Create;');
end;

destructor TTestClass.Destroy;
begin
   WriteLn('destructor TTestClass.Destroy;');
   inherited Destroy;
end;

procedure TTestClass.Test;
begin
   WriteLn('procedure TTestClass.Test;');
end;

initialization
   WriteLn('TestClassConstructorUnit.initialization');

finalization;
   WriteLn('TestClassConstructorUnit.finalization');
end.
