unit InstallerHelper.Options;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   IDEOptionsIntf,
   BaseIDEIntf,
   LazIDEIntf,
   IDEMsgIntf,
   IDEExternToolIntf,
   LazConfigStorage,
   InstallerHelper.Debug,
   InstallerHelper.InnoSetup.Options,
   InstallerHelper.NSIS.Options,
   InstallerHelper.WiX.Options;

type

   { TInstallerOptions }

   TInstallerOptions = class(TAbstractIDEEnvironmentOptions)
   private
      FInnoSetup: TInstallerInnoSetupOptions;
      FNSIS: TInstallerNSISOptions;
      FWiX: TInstallerWiXOptions;
      procedure LoadFromStorage(AStorage: TConfigStorage);
      procedure SaveToStorage(AStorage: TConfigStorage);
   private
      class var FInstance: TInstallerOptions;
   public
      class constructor Create;
      class destructor Destroy;
      class function GetGroupCaption: string; override;
      class function GetInstance: TAbstractIDEOptions; override;
   public
      constructor Create;
      destructor Destroy; override;
      procedure DoAfterWrite(Restore: boolean); override;
      procedure ReadXML;
      procedure WriteXML;
      property InnoSetup: TInstallerInnoSetupOptions read FInnoSetup;
      property NSIS: TInstallerNSISOptions read FNSIS;
      property WiX: TInstallerWiXOptions read FWiX;
   end;

implementation

{ TInstallerOptions }

procedure TInstallerOptions.LoadFromStorage(AStorage: TConfigStorage);
begin
   FInnoSetup.LoadFromStorage(AStorage);
   FNSIS.LoadFromStorage(AStorage);
   FWiX.LoadFromStorage(AStorage);
end;

procedure TInstallerOptions.SaveToStorage(AStorage: TConfigStorage);
begin
   FInnoSetup.SaveToStorage(AStorage);
   FNSIS.SaveToStorage(AStorage);
   FWiX.SaveToStorage(AStorage);
end;

class constructor TInstallerOptions.Create;
begin
   FInstance := nil;
end;

class destructor TInstallerOptions.Destroy;
begin
   FInstance.Free;
end;

class function TInstallerOptions.GetGroupCaption: string;
begin
   Result := 'Installer Toolkits';
end;

class function TInstallerOptions.GetInstance: TAbstractIDEOptions;
begin
   if not Assigned(FInstance) then begin
      FInstance := TInstallerOptions.Create;
   end;
   Result := FInstance;
end;

constructor TInstallerOptions.Create;
begin
   inherited Create;
   FInnoSetup := TInstallerInnoSetupOptions.Create;
   FNSIS := TInstallerNSISOptions.Create;
   FWiX := TInstallerWiXOptions.Create;
   ReadXML;
end;

destructor TInstallerOptions.Destroy;
begin
   WriteXML;
   FInnoSetup.Free;
   FNSIS.Free;
   FWiX.Free;
   inherited Destroy;
end;

procedure TInstallerOptions.DoAfterWrite(Restore: boolean);
begin
   InstallerHelperLogInformation(Self, 'TInstallerOptions.DoAfterWrite');
   inherited DoAfterWrite(Restore);
   WriteXML;
end;

procedure TInstallerOptions.ReadXML;
var
   config: TConfigStorage;
begin
   InstallerHelperLogInformation(Self, 'TInstallerOptions.ReadXML');
   try
      config := GetIDEConfigStorage('installeroptions.xml', True);
      try
         FInnoSetup.LoadFromStorage(config);
         FNSIS.LoadFromStorage(config);
         FWiX.LoadFromStorage(config);
         InstallerHelperLogInformation(Self, 'TInstallerOptions.ReadXML.InnoSetup.LibraryFilename = ' + FInnoSetup.LibraryFilename);
         InstallerHelperLogInformation(Self, 'TInstallerOptions.ReadXML.NSIS.ExecutableFilename = ' + FNSIS.ExecutableFilename);
         InstallerHelperLogInformation(Self, 'TInstallerOptions.ReadXML.WiX.ExecutableFilename = ' + FWiX.ExecutableFilename);
      finally
         config.Free;
      end;
   except
      on E: Exception do begin
         AddIDEMessage(mluError, 'Reading config: ' + E.Message);
      end;
   end;
end;

procedure TInstallerOptions.WriteXML;
var
   config: TConfigStorage;
begin
   InstallerHelperLogInformation(Self, 'TInstallerOptions.WriteXML');
   try
      config := GetIDEConfigStorage('installeroptions.xml', True);
      try
         InstallerHelperLogInformation(Self, 'TInstallerOptions.WriteXML.InnoSetup.LibraryFilename = ' + FInnoSetup.LibraryFilename);
         InstallerHelperLogInformation(Self, 'TInstallerOptions.WriteXML.NSIS.ExecutableFilename = ' + FNSIS.ExecutableFilename);
         InstallerHelperLogInformation(Self, 'TInstallerOptions.WriteXML.WiX.ExecutableFilename = ' + FWiX.ExecutableFilename);
         FInnoSetup.SaveToStorage(config);
         FNSIS.SaveToStorage(config);
         FWiX.SaveToStorage(config);
      finally
         config.WriteToDisk;
         config.Free;
      end;
   except
      on E: Exception do begin
         AddIDEMessage(mluError, 'Writing config: ' + E.Message);
      end;
   end;
end;

end.
