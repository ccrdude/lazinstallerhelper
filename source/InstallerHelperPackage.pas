{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit InstallerHelperPackage;

{$warn 5023 off : no warning about unused units}
interface

uses
  InstallerHelper.Menu, InstallerHelper.InnoSetup.Frame, 
  InstallerHelper.InnoSetup.Options, InstallerHelper.NSIS.Frame, 
  InstallerHelper.NSIS.Options, InstallerHelper.Options, 
  InstallerHelper.WiX.Frame, InstallerHelper.WiX.Options, 
  InstallerHelper.Debug, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('InstallerHelper.Menu', @InstallerHelper.Menu.Register);
end;

initialization
  RegisterPackage('InstallerHelperPackage', @Register);
end.
