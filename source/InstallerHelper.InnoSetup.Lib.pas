{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer support for InnoSetup.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2021 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2021-08-30  pk  30m  Now updating version number in InnoSetpu script.
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.InnoSetup.Lib;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Windows,
   ProjectIntf,
   LazIDEIntf,
   IDEMsgIntf,
   IDEExternToolIntf,
   CompInt;

type
   TOnInnoSetupMessageEvent = procedure(AMsg: string; ALineNumber: integer = 0) of object;

   PScriptLine = ^TScriptLine;

   TScriptLine = record
      LineText: string;
      Next: PScriptLine;
   end;
   { TInstallerHelperInnoSetupCompilerLibrary }

   TInstallerHelperInnoSetupCompilerLibrary = class
   private
      FLibraryFilename: string;
      FOnMessage: TOnInnoSetupMessageEvent;
      FOutputFilename: string;
      FScriptFilename: string;
      FViewName: string;
      FCurLine: String;
      FScriptLines: PScriptLine;
      FNextScriptLine: PScriptLine;
      FLastCompressValue: cardinal;
      function FindVersionNumber(AReplace: boolean = false; ANewVersion: string = ''): string;
      function GetVersionNumber: string;
      function ProcessCallback(ACode: integer; var AData: TCompilerCallbackData): integer;
      procedure ReadScriptLines(const ALines: TStringList);
      procedure SetVersionNumber(AValue: string);
   protected
      function DoMessage(TheUrgency: TMessageLineUrgency; AMsg: string; AFilename: string = ''; ALineNumber: integer = 0): TMessageLine;
   public
      constructor Create;
      function Build: boolean;
      property VersionNumber: string read GetVersionNumber write SetVersionNumber;
      property LibraryFilename: string read FLibraryFilename write FLibraryFilename;
      property ScriptFilename: string read FScriptFilename write FScriptFilename;
      property ViewName: string read FViewName write FViewName;
      property OutputFilename: string read FOutputFilename;
      property OnMessage: TOnInnoSetupMessageEvent read FOnMessage write FOnMessage;
   end;

implementation

uses
   PepiMK.Installer.InnoSetup;

function InnoSetupCompilerCallbackProc(ACode: integer; var AData: TCompilerCallbackData; AAppData: longint): integer; stdcall;
var
   isc: TInstallerHelperInnoSetupCompilerLibrary;
begin
   if AAppData = 0 then begin
      Exit;
   end;
   isc := TInstallerHelperInnoSetupCompilerLibrary(AAppData);
   Result := isc.ProcessCallback(ACode, AData);
end;


procedure TInstallerHelperInnoSetupCompilerLibrary.ReadScriptLines(const ALines: TStringList);
var
   iLineNumber: integer;
   plPrevLine, plCurrent: PScriptLine;
   i: integer;
begin
   iLineNumber := 1;
   plPrevLine := nil;
   for i := 0 to Pred(ALines.Count) do begin
      New(plCurrent);
      try
         plCurrent^.LineText := ALines[i];
         if Pos(#0, plCurrent^.LineText) <> 0 then begin
            raise Exception.CreateFmt('Illegal null character on line %d', [iLineNumber]);
         end;
         plCurrent^.Next := nil;
      except
         Dispose(plCurrent);
         raise;
      end;
      if Assigned(plPrevLine) then begin
         plPrevLine^.Next := plCurrent;
      end else begin
         FScriptLines := plCurrent;
         FNextScriptLine := plCurrent;
      end;
      plPrevLine := plCurrent;
      Inc(iLineNumber);
   end;
end;

procedure TInstallerHelperInnoSetupCompilerLibrary.SetVersionNumber(
  AValue: string);
begin
   FindVersionNumber(true, AValue);
end;

function TInstallerHelperInnoSetupCompilerLibrary.ProcessCallback(ACode: integer; var AData: TCompilerCallbackData): integer;
begin
   Result := iscrSuccess;
   case ACode of
      iscbReadScript:
      begin
         if Assigned(FNextScriptLine) then begin
           FCurLine := FNextScriptLine^.LineText;
           FNextScriptLine := FNextScriptLine^.Next;
           AData.LineRead := PChar(FCurLine);
         end;
         //if Assigned(AData.LineRead) then begin
         //   DoMessage(mluDebug, AData.LineRead);
         //end else begin
         //   DoMessage(mluDebug, 'End of file.');
         //end;
      end;
      iscbNotifyStatus:
      begin
         DoMessage(mluDebug, AData.StatusMsg);
      end;
      iscbNotifyIdle:
      begin
         if AData.CompressProgress <> FLastCompressValue then begin
            if AData.SecondsRemaining > 0 then begin
               DoMessage(mluProgress, Format('Compression: %d / %d, %d seconds remaining...', [AData.CompressProgress, AData.CompressProgressMax, AData.SecondsRemaining]));
            end else begin
               DoMessage(mluProgress, Format('Compression: %d / %d', [AData.CompressProgress, AData.CompressProgressMax]));
            end;
            FLastCompressValue := AData.CompressProgress;
         end;
      end;
      iscbNotifySuccess:
      begin
         FOutputFilename := AData.OutputExeFilename;
         DoMessage(mluImportant, Format('Installer created at %s.', [AData.OutputExeFilename]));
      end;
      iscbNotifyError:
      begin
         if Assigned(AData.ErrorMsg) then begin
            DoMessage(mluError, AData.ErrorMsg, AData.ErrorFilename, AData.ErrorLine);
         end else begin
            DoMessage(mluError, 'Installer error.', AData.ErrorFilename, AData.ErrorLine);
         end;
      end;
   end;
end;

function TInstallerHelperInnoSetupCompilerLibrary.FindVersionNumber(
  AReplace: boolean; ANewVersion: string): string;
var
   sl: TStringList;
   i: integer;
   sConst: string;
   bWrite: boolean;
begin
   Result := '';
   bWrite := false;
   if not FileExists(FScriptFilename) then begin
      Exit;
   end;
   sl := TStringList.Create;
   try
      sl.LoadFromFile(FScriptFilename);
      for i := 0 to Pred(sl.Count) do begin
         // AppVersion={#MyAppVersion}
         // ;AppVerName={#MyAppName} {#MyAppVersion}
         if Copy(sl[i],1,11) = 'AppVersion=' then begin
            Result := sl[i];
            Delete(Result, 1, 11);
            if AReplace and (Pos('{#', Result) = 0) then begin
               // todo : writenew version number
               sl[i] := 'AppVersion=' + ANewVersion;
               bWrite := true;
            end;
            break;
         end;
      end;
      if Pos('{#', Result) > 0 then begin
         sConst := Result;
         Delete(sConst, 1, Pos('{#', sConst) + 1);
         SetLength(sConst, Pos('}', sConst) - 1);
         for i := 0 to Pred(sl.Count) do begin
            // #define MyAppVersion "1.5"
            if Copy(sl[i],1,8 + Length(sConst)) = ('#define ' + sConst) then begin
               Result := sl[i];
               Delete(Result, 1, 8 + Length(sConst));
               Delete(Result, 1, Pos('"', Result));
               SetLength(Result, Pos('"', Result) - 1);
               if AReplace then begin
                  sl[i] := '#define ' + sConst + ' "' + ANewVersion + '"';
                  bWrite := true;
               end;
               break;
            end;
         end;
      end;
      if bWrite then begin
         sl.SaveToFile(FScriptFilename);
      end;
   finally
      sl.Free;
   end;
end;

function TInstallerHelperInnoSetupCompilerLibrary.GetVersionNumber: string;
begin
   Result := FindVersionNumber(false);
end;

function TInstallerHelperInnoSetupCompilerLibrary.DoMessage(TheUrgency: TMessageLineUrgency; AMsg: string; AFilename: string; ALineNumber: integer): TMessageLine;
begin
   if Length(AFilename) = 0 then begin
      AFilename := FScriptFilename;
   end;
   Result := AddIDEMessage(TheUrgency, AMsg, AFilename, ALineNumber, 0, FViewName);
   if Assigned(FOnMessage) then begin
      FOnMessage(AMsg, ALineNumber);
   end;
end;

constructor TInstallerHelperInnoSetupCompilerLibrary.Create;
begin
   FLastCompressValue := 0;
   FLibraryFilename := FindInnoSetupLibraryLocation();
end;

function TInstallerHelperInnoSetupCompilerLibrary.Build: boolean;
var
   h: THandle;
   fGetVersion: TISDllGetVersion;
   fCompile: TISDllCompileScriptA;
   pci: PCompilerVersionInfo;
   cp: TCompileScriptParamsExA;
   iResult: integer;
   sl: TStringList;
begin
   FOutputFilename := '';

   Result := False;
   sl := TStringList.Create;
   try
      sl.LoadFromFile(FScriptFilename);
      ReadScriptLines(sl);
      //AddIDEMessage(mluImportant, 'Building installer...', AFile.Filename, 0, 0, sView);
      try
         h := LoadLibrary(PChar(FLibraryFilename));
         if h > 0 then begin
            try
               fGetVersion := TISDllGetVersion(GetProcAddress(h, 'ISDllGetVersion'));
               if not Assigned(fGetVersion) then begin
                  DoMessage(mluError, Format('Unable to find ISDllGetVersion in %s', [FLibraryFilename]));
                  Exit;
               end;
               pci := fGetVersion();
               DoMessage(mluNote, pci^.Title + ' ' + pci^.Version);
               if pci^.BinVersion < $05000500 then begin
                  DoMessage(mluError, Format('Outdated version; need at least InnoSetup 5.0.5 at %s', [FLibraryFilename]));
                  Exit;
               end;
               fCompile := TISDllCompileScriptA(GetProcAddress(h, 'ISDllCompileScript'));
               if not Assigned(fCompile) then begin
                  DoMessage(mluError, Format('Unable to find ISDllCompileScript in %s', [FLibraryFilename]));
                  Exit;
               end;
               ZeroMemory(@cp, SizeOf(TCompileScriptParamsExA));
               cp.Size := SizeOf(TCompileScriptParamsExA);
               cp.SourcePath := PChar(ExtractFilePath(FScriptFilename));
               cp.AppData := longint(Self);
               cp.CallbackProc := @InnoSetupCompilerCallbackProc;
               cp.Options := nil;
               iResult := fCompile(cp);
               Result := (iResult = isceNoError);
               case iResult of
                  isceNoError:
                  begin
                     DoMessage(mluImportant, 'Installer build completed.');
                  end;
                  isceCompileFailure:
                  begin
                     DoMessage(mluError, 'Compiler failure.');
                  end;
                  isceInvalidParam:
                  begin
                     DoMessage(mluError, 'Invalid parameters.');
                  end;
                  else
                  begin
                     DoMessage(mluError, 'Unknown error.');
                  end;
               end;
            finally
               FreeLibrary(h);
            end;
         end else begin
            DoMessage(mluError, Format('Unable to load library at %s', [FLibraryFilename]));
            Exit;
         end;
      except
         on E: Exception do begin
            DoMessage(mluError, E.Message);
         end;
      end;
   finally
      sl.Free;
   end;
end;

end.
