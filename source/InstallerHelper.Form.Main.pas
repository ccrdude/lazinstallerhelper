{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Test form for testing Lazarus IDE installer extension.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.Form.Main;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   InstallerHelper.Menu,
   InstallerHelper.InnoSetup.Lib,
   InstallerHelper.NSIS.CommandLine,
   InstallerHelper.WiX.CommandLine;

type

   { TForm1 }

   TForm1 = class(TForm)
      bnInnoSetup: TButton;
      bnTestFileVersion: TButton;
      bnNSIS: TButton;
      bnWiX: TButton;
      Memo1: TMemo;
      procedure bnInnoSetupClick({%H-}Sender: TObject);
      procedure bnNSISClick({%H-}Sender: TObject);
      procedure bnTestFileVersionClick(Sender: TObject);
      procedure bnWiXClick(Sender: TObject);
   private
      { private declarations }
      procedure DoISMsg(AMsg: string; ALineNumber: integer = 0);
   public
      { public declarations }
   end;

var
   Form1: TForm1;

implementation

uses
   PepiMK.Installer.InnoSetup;

{$R *.lfm}

{ TForm1 }

procedure TForm1.bnInnoSetupClick(Sender: TObject);
var
   isc: TInstallerHelperInnoSetupCompilerLibrary;
begin
   Memo1.Lines.Clear;
   isc := TInstallerHelperInnoSetupCompilerLibrary.Create;
   try
      isc.ScriptFilename := ExtractFilePath(ParamStr(0)) + '..\..\source\test-innosetup.iss';
      isc.OnMessage := @DoISMsg;
      memo1.Lines.Add('Version (before): ' + isc.VersionNumber);
      isc.VersionNumber := '1.5.' + IntToStr(Random(99));
      memo1.Lines.Add('Version (after): ' + isc.VersionNumber);
      isc.Build;
   finally
      isc.Free;
   end;
end;

procedure TForm1.bnNSISClick(Sender: TObject);
var
   int: TInstallerHelperNSISCompilerTool;
begin
   Memo1.Lines.Clear;
   int := TInstallerHelperNSISCompilerTool.Create;
   try
      int.ScriptFilename := ExtractFilePath(ParamStr(0)) + '..\..\source\test-nsis.nsi';
      int.OnMessage := @DoISMsg;
      int.Build;
   finally
      int.Free;
   end;
end;

procedure TForm1.bnTestFileVersionClick(Sender: TObject);
begin
   TInstallerHelper.DumpFileInformation('Hallo %s: %s', FindInnoSetupLibraryLocation(), 'bla');
end;

procedure TForm1.bnWiXClick(Sender: TObject);
var
   iw: TInstallerHelperWiXTool;
begin
   Memo1.Lines.Clear;
   iw := TInstallerHelperWiXTool.Create;
   try
      iw.ScriptFilename := ExtractFilePath(ParamStr(0)) + '..\..\source\test-wix.wxs';
      iw.OnMessage := @DoISMsg;
      iw.Build;
   finally
      iw.Free;
   end;
end;

procedure TForm1.DoISMsg(AMsg: string; ALineNumber: integer);
begin
   if ALineNumber > 0 then begin
      Memo1.Lines.Add('Line ' + IntToStr(ALineNumber) + ': ' + AMsg);
   end else begin
      Memo1.Lines.Add(AMsg);
   end;
end;

end.
