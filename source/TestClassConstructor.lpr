program TestClassConstructor;

{$mode objfpc}{$H+}

uses
   SysUtils,
   Classes,
   TestClassConstructorUnit;

begin
   WriteLn('TestClassConstructor.begin#1');
   TTestClass.Instance.Test;
   WriteLn('TestClassConstructor.begin#2');
end.
