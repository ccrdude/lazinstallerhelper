unit TestMenuItemVisibilityMain;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   LazIDEIntf,
   IDEMsgIntf,
   IDEExternToolIntf,
   ProjectIntf,
   MenuIntf;

type

   { TMenuItemTest }

   TMenuItemTest = class
   private
      class var FInstance: TMenuItemTest;
   private
      FMenuItem: TIDEMenuItem;
      procedure DoShowOtherClick(Sender: TObject);
      procedure DoHideMeClick(Sender: TObject);
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: TMenuItemTest;
   public
      destructor Destroy; override;
      procedure Initialize;
   end;

procedure Register;

implementation

procedure Register;
begin
   TMenuItemTest.Instance.Initialize;
end;

{ TMenuItemTest }

procedure TMenuItemTest.DoShowOtherClick(Sender: TObject);
begin
   AddIDEMessage(mluImportant, 'TMenuItemTest.DoShowOtherClick');
   FMenuItem.Visible := True;
end;

procedure TMenuItemTest.DoHideMeClick(Sender: TObject);
begin
   AddIDEMessage(mluImportant, 'TMenuItemTest.DoHideMeClick');
   FMenuItem.Visible := False;
end;

class constructor TMenuItemTest.Create;
begin
   FInstance := nil;
end;

class destructor TMenuItemTest.Destroy;
begin
   FInstance.Free;
end;

class function TMenuItemTest.Instance: TMenuItemTest;
begin
   if not Assigned(FInstance) then begin
      FInstance := TMenuItemTest.Create;
   end;
   Result := FInstance;
end;

destructor TMenuItemTest.Destroy;
begin
   inherited Destroy;
end;

procedure TMenuItemTest.Initialize;
begin
   FMenuItem := RegisterIDEMenuCommand(mnuMain, 'MenuItemTest1', 'HideMe', @DoHideMeClick);
   RegisterIDEMenuCommand(mnuMain, 'MenuItemTest2', 'ShowOther', @DoShowOtherClick);
end;

end.
