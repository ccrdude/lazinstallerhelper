{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer building support for InnoSetup.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2021 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2021-08-30  pk   5m  Added InnoSetup 6 location
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit PepiMK.Installer.InnoSetup;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils;

function FindInnoSetupLibraryLocation(): string;

implementation

uses
   registry;

function DetectInnoSetupLocation(out APath: string): boolean;
var
   reg: TRegistry;
begin
   Result := False;
   try
      reg := TRegistry.Create;
      try
         reg.RootKey := HKEY_LOCAL_MACHINE;
         if reg.OpenKey('\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Inno Setup 6_is1', False) then begin
            try
               APath := reg.ReadString('InstallLocation');
               Result := True;
            finally
               reg.CloseKey();
            end;
         end else if reg.OpenKey('\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Inno Setup 5_is1', False) then begin
            try
               APath := reg.ReadString('InstallLocation');
               Result := True;
            finally
               reg.CloseKey();
            end;
         end;
      finally
         reg.Free;
      end;
   except
      Result := False;
   end;
end;

function FindInnoSetupLibraryLocation(): string;
var
   s: string;
begin
   if DetectInnoSetupLocation(s) then begin
      Result := IncludeTrailingPathDelimiter(s) + 'ISCmplr.dll';
      Exit;
   end;
   Result := 'c:\Program Files (x86)\Inno Setup 5\ISCmplr.dll';
end;

end.
