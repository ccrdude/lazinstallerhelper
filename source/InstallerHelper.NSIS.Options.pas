{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Installer options for NSIS.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.NSIS.Options;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   LazIDEIntf,
   //IDEOptionsIntf,
   LazConfigStorage,
   IniFiles;

type

   { TInstallerNSISOptions }

   TInstallerNSISOptions = class(TPersistent)
   private
      FAutoCreate: boolean;
      FAutoSign: boolean;
      FExecutableFilename: string;
      FConfigFilename: string;
   public
      constructor Create;
      destructor Destroy; override;
      procedure LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASectionName: string);
      procedure LoadFromStorage(AStorage: TConfigStorage);
      procedure SaveToStorage(AStorage: TConfigStorage);
      procedure Assign(Source: TPersistent); override;
      property AutoCreate: boolean read FAutoCreate write FAutoCreate;
      property AutoSign: boolean read FAutoSign write FAutoSign;
      property ExecutableFilename: string read FExecutableFilename write FExecutableFilename;
   end;

implementation

uses
   //IDEMsgIntf,
   IDEExternToolIntf,
   PepiMK.Installer.NSIS;

{ TInstallerNSISOptions }

procedure TInstallerNSISOptions.LoadFromIni(AIni: TCustomIniFile; ASectionName: string);
begin
   FAutoCreate := AIni.ReadBool(ASectionName, 'AutoCreate', False);
   FAutoSign := AIni.ReadBool(ASectionName, 'AutoSign', False);
   FExecutableFilename := AIni.ReadString(ASectionName, 'ExecutableFilename', UTF8Encode(FExecutableFilename));
end;

procedure TInstallerNSISOptions.SaveToIni(AIni: TCustomIniFile; ASectionName: string);
begin
   AIni.WriteBool(ASectionName, 'AutoCreate', FAutoCreate);
   AIni.WriteBool(ASectionName, 'AutoSign', False);
   AIni.WriteString(ASectionName, 'ExecutableFilename', UTF8Encode(FExecutableFilename));
end;

procedure TInstallerNSISOptions.LoadFromStorage(AStorage: TConfigStorage);
begin
   FExecutableFilename := AStorage.GetValue('NSIS/ExecutableFilename', '');
   FAutoCreate := AStorage.GetValue('NSIS/AutoCreate', false);
   FAutoSign := AStorage.GetValue('NSIS/AutoSign', false);
end;

procedure TInstallerNSISOptions.SaveToStorage(AStorage: TConfigStorage);
begin
   AStorage.SetDeleteValue('NSIS/ConfigVersion', 1, 0);
   AStorage.SetDeleteValue('NSIS/ExecutableFilename', FExecutableFilename, '');
   AStorage.SetDeleteValue('NSIS/AutoCreate', FAutoCreate, false);
   AStorage.SetDeleteValue('NSIS/AutoSign', FAutoSign, false);
end;

constructor TInstallerNSISOptions.Create;
begin
   inherited Create;
   FAutoCreate := False;
   FAutoSign := False;
   FExecutableFilename := FindNSISExecutableLocation;
end;

destructor TInstallerNSISOptions.Destroy;
begin
   inherited Destroy;
end;

procedure TInstallerNSISOptions.Assign(Source: TPersistent);
begin
   if Source is TInstallerNSISOptions then begin
      inherited Assign(Source);
      Self.ExecutableFilename := TInstallerNSISOptions(Source).ExecutableFilename;
      Self.AutoSign := TInstallerNSISOptions(Source).AutoSign;
      Self.AutoCreate := TInstallerNSISOptions(Source).AutoCreate;
   end else begin
      inherited Assign(Source);
   end;
end;

end.
