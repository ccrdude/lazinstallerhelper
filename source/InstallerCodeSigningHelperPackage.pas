{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit InstallerCodeSigningHelperPackage;

interface

uses
   InstallerCodeSigningHelper.Menu, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('InstallerCodeSigningHelper.Menu', 
     @InstallerCodeSigningHelper.Menu.Register);
end;

initialization
  RegisterPackage('InstallerCodeSigningHelperPackage', @Register);
end.
