{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-06  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit InstallerHelper.InnoSetup.Frame;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   StdCtrls,
   EditBtn,
   IDEOptEditorIntf,
   IDEOptionsIntf,
   InstallerHelper.Options,
   InstallerHelper.InnoSetup.Options;

type

   { TFrameInstallerOptionsInnoSetup }

   TFrameInstallerOptionsInnoSetup = class(TAbstractIDEOptionsEditor)
      cbAutoSign: TCheckBox;
      cbAutoBuild: TCheckBox;
      editLibrary: TFileNameEdit;
      groupAutomation: TGroupBox;
      groupLibrary: TGroupBox;
   public
      { public declarations }
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings({%H-}AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings({%H-}AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

{$R *.lfm}

{ TFrameInstallerOptionsInnoSetup }

function TFrameInstallerOptionsInnoSetup.GetTitle: string;
begin
   Result := 'InnoSetup';
end;

procedure TFrameInstallerOptionsInnoSetup.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   cbAutoSign.Enabled := Assigned(FindClass('TInstallerCodeSigningHelper'));
   ReadSettings(TInstallerOptions.GetInstance);
end;

procedure TFrameInstallerOptionsInnoSetup.ReadSettings(AOptions: TAbstractIDEOptions);
var
   o: TInstallerInnoSetupOptions;
begin
   TInstallerOptions(TInstallerOptions.GetInstance).ReadXML;
   o := TInstallerOptions(TInstallerOptions.GetInstance).InnoSetup;
   cbAutoBuild.Checked := o.AutoCreate;
   cbAutoSign.Checked := o.AutoSign;
   editLibrary.Text := UTF8Encode(o.LibraryFilename);
end;

procedure TFrameInstallerOptionsInnoSetup.WriteSettings(AOptions: TAbstractIDEOptions);
var
   o: TInstallerInnoSetupOptions;
begin
   o := TInstallerOptions(TInstallerOptions.GetInstance).InnoSetup;
   o.AutoCreate := cbAutoBuild.Checked;
   o.AutoSign := cbAutoSign.Checked;
   o.LibraryFilename := editLibrary.Text;
   TInstallerOptions(TInstallerOptions.GetInstance).WriteXML;
end;

class function TFrameInstallerOptionsInnoSetup.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TInstallerOptions;
end;

end.
